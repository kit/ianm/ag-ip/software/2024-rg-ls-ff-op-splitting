# 2024 Rg Ls ff op splitting

## License

Copyright (c) 2024, Roland Griesmaier, Lisa Schätzle

This software is released under GNU General Public License, Version 3.
The full license text is provided in the file LICENSE included in this repository
or can be obtained from http://www.gnu.org/licenses/

## Content of this project

This is a guide to generate the figures and tables that have been used in the work

**"Far field operator splitting and completion in inverse medium scattering"**

by Roland Griesmaier and Lisa Schätzle.

The following versions of the paper are available:

- [ ] [CRC Preprint 2024/3, February 2024](https://www.waves.kit.edu/downloads/CRC1173_Preprint_2024-3.pdf)

You find all needed Matlab files to generate the figures and tables.

## Requirements

The following additional software is necessary to run the code in this project:

- [ ] a recent version of Matlab

## An overview

- [ ] *addNoise.m* adds p% complex valued uniformly distributed additive error to a matrix.
- [ ] *applyRP.m* completes a matrix according to the symmetrie resulting from the reciprocity relation.
- [ ] *CG_secondOrder.m* solves the least squares problem for the splitting and/ or completion problem in Born approximation of order 2 numerically by using the cg method.
- [ ] *evaluateFarfieldNystrom.m* evaluates the far field patterns for n2 incident and observation directions on an equidistant grid on the unit sphere for each configuration, i.e. simulates the far field operator, by using a Nyström method.
- [ ] *evaluateFarfieldSecondOrder.m* Simulates the Born far field operator of order 2 by using trigonometric interpolation.
- [ ] *example_5_1.m* tests both methods for the splitting problem and two scatterers, adding 5% equally distributed random noise to the synthetic data for two different szenarios (varying distance, varying size of one scatterer).
- [ ] *example_5_2.m* tests both methods for the completion problem and two scatterers, adding 5% equally distributed random noise to the synthetic data for two different geometric setups for Omega.
- [ ] *example_5_3.m* tests both methods for the completion and splitting problem and two scatterers, adding 5% equally distributed random noise to the synthetic data for two different geometric setups for Omega.
- [ ] *figure_2_1.m* provides plots of the factors causing the essential decay behaviour of the expansion coefficients of far field operators for different choices of kR.
- [ ] *figure_2_2.m* provides plots of the absolute values of the expansion coefficients of a far field operator as well as of the geometry of the corresponding scatterer.
- [ ] *figure_3_1.m* provides plots of the absolute values of the expansion coefficients of a Born far field operator of order 2 as well as of the geometry of the corresponding two scatterers.
- [ ] *FISTA_secondOrder.m* solves the l1xl1 minimization problem for the splitting and/ or completion problem in Born approximation of order 2 numerically by using fast iterative thresholding.
- [ ] *generateOmega.m* simulates projection operator corresponding to some missing data segment Omega.
- [ ] *kurve.m* provides curve values for different shapes of scatterers.
- [ ] *MyCMap.mat* provides colormap for the plots of expansion coefficients of far field operators.
- [ ] *projOp.m* simulates projection operator on generalized subspace of non-evanescent far field operators.
- [ ] *softshrink.m* applies soft-shrinkage operator to a matrix.
- [ ] *translOp.m* applies generalized translation operator to a far field matrix.

## Generating the files

For generating the Figures 2.1, 2.2 and 3.1 from the work, run

* *figure_2_1.m* - **Figure 2.1** (Plots of the factors causing the decay behaviour of the expansion coefficients)
* *figure_2_2.m* -  **Figure 2.2** (Example 2.6, geometry of kite-shaped scatterer, expansion coefficients of corr. far field operator)
* *figure_3_1.m* -  **Figure 3.1** (Example 3.4, geometry of kite- and nut-shaped scatterer, expansion coefficients of corr. Born far field operator of order 2)

For generating the Figures 5.1 and 5.2, as well as the data included in the Tables 5.1, 5.2 and 5.3 from the work, run

* *example_5_1.m* - **Figure 5.1**, **Figure 5.2** (Example 5.1, splitting only for two diifferent szenarios, varying distance and size of one scatterer)
* *example_5_2.m* - **Figure 5.3**, **Figure 5.4** (Example 5.2, completion for two diifferent geometrical setups for Omega)
* *example_5_3.m* - **Figure 5.5** (Example 5.3, completion and splitting for two diifferent geometrical setups for Omega)
