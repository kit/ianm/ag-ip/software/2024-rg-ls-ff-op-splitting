%% Example 5.3 (Completion and Splitting) for Omega_1:

clear all
close all
clc

% Parameters:

k = .5;  % wave number

sampling.nxhat = 2^8;  % number of observation directions, i.e. number of rows 
sampling.nd = 2^8; % number of illumination directions, i.e. number of columns
sampling.xhat = (0:sampling.nxhat-1)'/sampling.nxhat*2*pi; % detector positions
sampling.d = (0:sampling.nd-1)/sampling.nd*2*pi; % illumination directions

nr_of_scatterers = 2;
q = [-.5, 1];

noiselevel = 0.05;

alpha_vec = pi/32*[1,2,3,4,5,6,7,8,9,10]; % controlls area of non-observable sets

nr_rep = length(alpha_vec);

P_Omega = cell(1,nr_rep);
P_OmegaC = cell(1,nr_rep);

for iteri=1:nr_rep

    alpha = alpha_vec(iteri);

    % cross-shaped Omega, 20% midding data:
    OmegaTypes = {'constant_height','constant_width'};
    par = [pi/3, 0, alpha, 0, 0; 0, 4*pi/3, 0, alpha, 0];

    [Help, ~] = generateOmega(OmegaTypes, sampling.nxhat, sampling.nd, par);

    % To enforce symmetry according to reciprocity principle:
    P_Omega{iteri} = applyRP(Help);
    P_OmegaC{iteri} = ones(sampling.nd,sampling.nxhat) - P_Omega{iteri};

    ratio(iteri) = sum(sum(P_Omega{iteri}))/(sampling.nxhat*sampling.nd);

    clear Help
end

% Simulate far field operators:

znut = [26;-3];
v = [-2;-1]/norm([-2;-1]);
dist = 20;
zkite = znut+dist*v;

z = [znut, round(zkite)];
R = [3.5, 5];  % radii of circles containing scatterer components

G = cell(1,nr_rep);

params = [1 .5 znut.' 45 3; 1 .5 zkite.' 135 3];

[Fall, ~, ~] = evaluateFarfieldNystrom({'nut', 'kite'}, params, q, k, sampling.nxhat, 0);
    
for iteri = 1:nr_rep
    rng("default")
    G{iteri} = P_OmegaC{iteri} .* addNoise(Fall, noiselevel);
end

% Numerical reconstruction with least squares approach:

tol = [1e-5 1e-5 1e-5 1e-4 1e-4 5e-3 5e-3 5e-3 5e-3 1e-3];
kmax = 300;

Anut = cell(1,nr_rep);
Akite = cell(1,nr_rep);
Aall = cell(1,nr_rep);
B = cell(1,nr_rep);

indOfInterest = sub2ind([nr_of_scatterers,nr_of_scatterers],1:nr_of_scatterers,1:nr_of_scatterers);

for iteri = 1:nr_rep
   [A, ~, ~] = CG_secondOrder(G{iteri}, sampling, k, P_Omega{iteri}, z, R, kmax, tol(iteri));

   Aall{iteri} = A{2} + A{3} + A{4} + A{5};
   A = {A{1},A{indOfInterest+1}};
   B{iteri} = A{1};
   Anut{iteri} = A{2};
   Akite{iteri} = A{3};

   clear A

   relerrOmega1_cg(iteri) = norm(B{iteri}+P_Omega{iteri}.*Fall)/norm(P_Omega{iteri}.*Fall);
   relerr1_cg(iteri) = norm(Aall{iteri}-Fall)/norm(Fall);
end

% Numerical reconstruction with l1xl1-minimization:

kmax = 300;

Anut = cell(1,nr_rep);
Akite = cell(1,nr_rep);
Aall = cell(1,nr_rep);
B = cell(1,nr_rep);

for iteri = 1:nr_rep
    [A, ~, ~] = FISTA_secondOrder(G{iteri}, sampling, k, P_OmegaC{iteri}, z, kmax);
    Aall{iteri} = A{1};
    Anut{iteri} = A{2};
    Akite{iteri} = A{3};

    clear A

    relerrOmega1_fista(iteri) = norm(P_Omega{iteri}.*(Aall{iteri}-Fall))/norm(P_Omega{iteri}.*Fall);
    relerr1_fista(iteri) = norm(Aall{iteri}-Fall)/norm(Fall);
end

%% Plot of relative errors for Omega_1:

figure()

semilogy(ratio, relerrOmega1_cg, 'r--o', ratio, relerr1_cg, 'b--*', ratio, relerrOmega1_fista, 'r-o', ratio, relerr1_fista, 'b-*', 'LineWidth', 1.2)

title('Relative errors for varying area of non-observable set $\Omega_1$', 'Interpreter', 'latex')
xlabel('$\frac{|\Omega|}{4\pi^2}$', 'Interpreter', 'latex')
ylabel('$\epsilon_{\mathrm{rel}}$', 'Interpreter', 'latex')

legend({'$\epsilon_{\mathrm{rel}}^{\Omega}$ using cg','$\epsilon_{\mathrm{rel}}$ using cg', '$\epsilon_{\mathrm{rel}}^{\Omega}$ using FISTA', '$\epsilon_{\mathrm{rel}}$ using FISTA'}, 'Interpreter','latex', 'Location','southeast')

xlim([ratio(1) ratio(end)])
ylim([10^(-2) 1])

grid on

ax = gca;
ax.FontSize = 16;

print ../figures/errors_vary_Omega1_completionSplitting.eps -depsc

%% Example 5.3 (Completion and Splitting) for Omega_2:

clear all

% Parameters:

k = .5;  % wave number

sampling.nxhat = 2^8;  % number of observation directions, i.e. number of rows 
sampling.nd = 2^8; % number of illumination directions, i.e. number of columns
sampling.xhat = (0:sampling.nxhat-1)'/sampling.nxhat*2*pi; % detector positions
sampling.d = (0:sampling.nd-1)/sampling.nd*2*pi; % illumination directions

nr_of_scatterers = 2;
q = [-.5, 1];

noiselevel = 0.05;

alpha_vec = pi/32*[1,2,3,4,5,6,7,8,9,10]; % controlls area of non-observable sets

nr_rep = length(alpha_vec);

P_Omega = cell(1,nr_rep);
P_OmegaC = cell(1,nr_rep);

for iteri=1:nr_rep

    alpha = alpha_vec(iteri);

    % Omega consisting of three components, 20% missing data:
    OmegaTypes = {'rectangle','rectangle','rectangle'};
    par = [pi/3, 0, alpha, pi, 0; pi, 4*pi/3, pi, alpha, 0; (3*pi-sqrt(alpha*(2*pi-alpha)))/2, (pi-sqrt(alpha*(2*pi-alpha)))/2, sqrt(alpha*(2*pi-alpha)), sqrt(alpha*(2*pi-alpha)), 0];

    [Help, ~] = generateOmega(OmegaTypes, sampling.nxhat, sampling.nd, par);

    % To enforce symmetry according to reciprocity principle:
    P_Omega{iteri} = applyRP(Help);
    P_OmegaC{iteri} = ones(sampling.nd,sampling.nxhat) - P_Omega{iteri};

    ratio(iteri) = sum(sum(P_Omega{iteri}))/(sampling.nxhat*sampling.nd);

    clear Help
end

% Simulate far field operators:

znut = [26;-3];
v = [-2;-1]/norm([-2;-1]);
dist = 20;
zkite = znut+dist*v;

z = [znut, round(zkite)];
R = [3.5, 5];  % radii of circles containing scatterer components

G = cell(1,nr_rep);

params = [1 .5 znut.' 45 3; 1 .5 zkite.' 135 3];

[Fall, ~, ~] = evaluateFarfieldNystrom({'nut', 'kite'}, params, q, k, sampling.nxhat, 0);
    
for iteri = 1:nr_rep
    rng("default")
    G{iteri} = P_OmegaC{iteri} .* addNoise(Fall, noiselevel);
end

% Numerical reconstruction with least squares approach:

tol = [1e-5 1e-5 1e-5 1e-4 1e-4 5e-3 5e-3 1e-3 1e-3 1e-3];
kmax = 300;

Anut = cell(1,nr_rep);
Akite = cell(1,nr_rep);
Aall = cell(1,nr_rep);
B = cell(1,nr_rep);

indOfInterest = sub2ind([nr_of_scatterers,nr_of_scatterers],1:nr_of_scatterers,1:nr_of_scatterers);

for iteri = 1:nr_rep
   [A, ~, ~] = CG_secondOrder(G{iteri}, sampling, k, P_Omega{iteri}, z, R, kmax, tol(iteri));

   Aall{iteri} = A{2} + A{3} + A{4} + A{5};
   A = {A{1},A{indOfInterest+1}};
   B{iteri} = A{1};
   Anut{iteri} = A{2};
   Akite{iteri} = A{3};

   clear A

   relerrOmega2_cg(iteri) = norm(B{iteri}+P_Omega{iteri}.*Fall)/norm(P_Omega{iteri}.*Fall);
   relerr2_cg(iteri) = norm(Aall{iteri}-Fall)/norm(Fall);
end

% Numerical reconstruction with l1xl1-minimization:

kmax = 300;

Anut = cell(1,nr_rep);
Akite = cell(1,nr_rep);
Aall = cell(1,nr_rep);
B = cell(1,nr_rep);

for iteri = 1:nr_rep
    [A, ~, ~] = FISTA_secondOrder(G{iteri}, sampling, k, P_OmegaC{iteri}, z, kmax);
    Aall{iteri} = A{1};
    Anut{iteri} = A{2};
    Akite{iteri} = A{3};

    clear A

    relerrOmega2_fista(iteri) = norm(P_Omega{iteri}.*(Aall{iteri}-Fall))/norm(P_Omega{iteri}.*Fall);
    relerr2_fista(iteri) = norm(Aall{iteri}-Fall)/norm(Fall);
end

%% Plot of relative errors for Omega_2:

figure()

semilogy(ratio, relerrOmega2_cg, 'r--o', ratio, relerr2_cg, 'b--*', ratio, relerrOmega2_fista, 'r-o', ratio, relerr2_fista, 'b-*', 'LineWidth', 1.2)

title('Relative errors for varying area of non-observable set $\Omega_2$', 'Interpreter', 'latex')
xlabel('$\frac{|\Omega|}{4\pi^2}$', 'Interpreter', 'latex')
ylabel('$\epsilon_{\mathrm{rel}}$', 'Interpreter', 'latex')

legend({'$\epsilon_{\mathrm{rel}}^{\Omega}$ using cg','$\epsilon_{\mathrm{rel}}$ using cg', '$\epsilon_{\mathrm{rel}}^{\Omega}$ using FISTA', '$\epsilon_{\mathrm{rel}}$ using FISTA'}, 'Interpreter','latex', 'Location','southeast')

xlim([ratio(1) ratio(end)])

grid on

ax = gca;
ax.FontSize = 16;

print ../figures/errors_vary_Omega2_completionSplitting.eps -depsc