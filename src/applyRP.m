function P_Omega2 = applyRP(P_Omega)

%% APPLYRP: Completes a matrix according to the symmetrie resulting from the reciprocity relation.
%
% INPUT:    P_Omega     Projection operator corresponding to non-observable part Omega, nxhat*nd-array
%                       with only 0 and 1 entries.
% OUTPUT    P_Omega2    Completed version of P_Omega, nxhat*nd-array, with only 0 and 1 entries.
%
% SYNTAX: applyRP(P_Omega)
%
% *****************************************************************************************************

[nxhat, nd] = size(P_Omega);

% Projection onto according to the reciprocity principle non-completable part of Omega:
P_Omega_shifted=zeros(nxhat,nd);
P_Omega2=zeros(nxhat,nd);
P_Omega_shifted(1:nxhat/2,:) = P_Omega(nxhat/2+1:nxhat,:);
P_Omega_shifted(nxhat/2+1:nxhat,:) = P_Omega(1:nxhat/2,:);
P_Omega2_shifted = P_Omega_shifted .* P_Omega_shifted.';
P_Omega2(1:nxhat/2,:) = P_Omega2_shifted(nxhat/2+1:nxhat,:);
P_Omega2(nxhat/2+1:nxhat,:) = P_Omega2_shifted(1:nxhat/2,:);
clear P_Omega_shifted P_Omega1_shifted

% Projection onto according to the reciprocity principle completable part of Omega:
P_Omega1 = P_Omega - P_Omega2;
P_Omega1_shifted = zeros(nxhat,nd);
P_Omega1_shifted(1:nxhat/2,:) = P_Omega1(nxhat/2+1:nxhat,:);
P_Omega1_shifted(nxhat/2+1:nxhat,:) = P_Omega1(1:nxhat/2,:);

% Calculation of the entries of G that can be completed:
P_Omega1_mirrored_shifted = P_Omega1_shifted.';
P_Omega1_mirrored = zeros(nxhat,nd);
P_Omega1_mirrored(1:nxhat/2,:) = P_Omega1_mirrored_shifted(nxhat/2+1:nxhat,:);
P_Omega1_mirrored(nxhat/2+1:nxhat,:) = P_Omega1_mirrored_shifted(1:nxhat/2,:);
clear P_Omega1_mirrored_shifted P_Omega1_mirrored

end
