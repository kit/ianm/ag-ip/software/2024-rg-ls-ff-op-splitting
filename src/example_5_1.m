%% Example 5.1 (Splitting only) for varying distance:

clear all
close all
clc

% Parameters:

k = .5;  % wave number

sampling.nxhat = 2^8;  % number of observation directions, i.e. number of rows 
sampling.nd = 2^8; % number of illumination directions, i.e. number of columns
sampling.xhat = (0:sampling.nxhat-1)'/sampling.nxhat*2*pi; % detector positions
sampling.d = (0:sampling.nd-1)/sampling.nd*2*pi; % illumination directions

nr_of_scatterers = 2;
q = [-.5, 1];

noiselevel = 0.05;

% Splitting only:
[P_Omega, P_OmegaC] = generateOmega({}, sampling.nxhat, sampling.nd, [0 0 0 0 0]);

% Vary distance:
R = [3.5, 5];  % radii of circles containing source components

znut = [26;-3];
v = [-2;-1]/norm([-2;-1]);
dist = [10 15 20 25 30 35 40 45];
zkite = zeros(2,length(dist));

nr_rep = length(dist);

% Simulate far field operators:

G = cell(1,nr_rep);
Fnut = cell(1,nr_rep);
Fkite = cell(1,nr_rep);
Fall = cell(1,nr_rep);

for iteri = 1:nr_rep
    zkite(:,iteri)  = znut+dist(iteri)*v;
    params = [1 .5 znut.' 45 3; 1 .5 [zkite(:,iteri)].' 135 3];
    
    [Fnut{iteri}, ~, ~] = evaluateFarfieldNystrom({'nut'}, params(1,:), q(1), k, sampling.nxhat, 0);
    [Fkite{iteri}, ~, ~] = evaluateFarfieldNystrom({'kite'}, params(2,:), q(2), k, sampling.nxhat, 0);
    [Fall{iteri}, ~, ~] = evaluateFarfieldNystrom({'nut', 'kite'}, params, q, k, sampling.nxhat, 0);
    
    clear params
    
    rng("default") % enusures same noise matrix in both test cases
    G{iteri} = P_OmegaC .* addNoise(Fall{iteri}, noiselevel);
end

% Numerical reconstruction with least squares approach:

tol = 1e-8;
kmax = 300;

Anut = cell(1,nr_rep);
Akite = cell(1,nr_rep);
Aall = cell(1,nr_rep);
B = cell(1,nr_rep);

indOfInterest = sub2ind([nr_of_scatterers,nr_of_scatterers],1:nr_of_scatterers,1:nr_of_scatterers);

for iteri = 1:nr_rep
   z = [znut, zkite(:,iteri)];

   [A, ~, ~] = CG_secondOrder(G{iteri}, sampling, k, P_Omega, z, R, kmax, tol);

   Aall{iteri} = A{2} + A{3} + A{4} + A{5};
   A = {A{1},A{indOfInterest+1}};
   B{iteri} = A{1};
   Anut{iteri} = A{2};
   Akite{iteri} = A{3};

   clear A z

   relerrnut1_cg(iteri) = norm(Anut{iteri}-Fnut{iteri})/norm(Fnut{iteri});
   relerrkite1_cg(iteri) = norm(Akite{iteri}-Fkite{iteri})/norm(Fkite{iteri});
end

% Numerical reconstruction with l1xl1-minimization:

kmax = 300;

Anut = cell(1,nr_rep);
Akite = cell(1,nr_rep);
Aall = cell(1,nr_rep);
B = cell(1,nr_rep);

for iteri = 1:nr_rep
    z = [znut, zkite(:,iteri)];

    [A, ~, ~] = FISTA_secondOrder(G{iteri}, sampling, k, P_OmegaC, z, kmax);
    Aall{iteri} = A{1};
    Anut{iteri} = A{2};
    Akite{iteri} = A{3};

    clear A z

    relerrnut1_fista(iteri) = norm(Anut{iteri}-Fnut{iteri})/norm(Fnut{iteri});
    relerrkite1_fista(iteri) = norm(Akite{iteri}-Fkite{iteri})/norm(Fkite{iteri});
end

% Plot relative errors:

figure()

semilogy(dist, relerrnut1_cg, 'r--o', dist, relerrkite1_cg, 'b--*', dist, relerrnut1_fista, 'r-o', dist, relerrkite1_fista, 'b-*', 'LineWidth', 1.2)

title('Relative errors for varying distance', 'Interpreter', 'latex')
xlabel('$|c_1-c_2|$', 'Interpreter', 'latex')
ylabel('$\epsilon_{\mathrm{rel}}$', 'Interpreter', 'latex')

legend({'$\epsilon_{\mathrm{rel}}^1$ using cg','$\epsilon_{\mathrm{rel}}^2$ using cg', '$\epsilon_{\mathrm{rel}}^1$ using FISTA', '$\epsilon_{\mathrm{rel}}^2$ using FISTA'}, 'Interpreter','latex', 'Location','northeast')

xlim([dist(1) dist(end)])
ylim([10^(-2) 1])

grid on

ax = gca;
ax.FontSize = 16;

print ../figures/errors_vary_distance.eps -depsc

% Plot of geometry:

figure()

d = (0:100)/100*2*pi;

% Nut:
[x_nut,~,~,~] = kurve(100,'nut', [1 .5 znut.' 45 3]);
x_nut = [x_nut;x_nut(1,:)];
plot(x_nut(:,1),x_nut(:,2),'LineWidth', 1.5, 'Color', 'blue')
hold on
scatter(znut(1,1),znut(2,1),100,'Marker','+','LineWidth', 1.2,'MarkerEdgeColor', 'black')
hold on
plot(3.5*cos(d)+26,3.5*sin(d)-3,'LineStyle', '--', 'LineWidth', 1.2, 'Color', 'black')
hold on

% Other kites:
for iteri = 1:nr_rep
    if iteri~=7
        zhelp = zkite(:,iteri);
        paramshelp = [1 .5 zhelp.' 135 3];

        [x_kite,~,~,~] = kurve(100, 'kite', paramshelp);
        x_kite = [x_kite;x_kite(1,:)];
        plot(x_kite(:,1),x_kite(:,2),'LineWidth', 1.5, 'Color', [0.678 0.847 0.902])
        hold on
        plot(5*cos(d)+zhelp(1) ,5*sin(d)+zhelp(2),'LineStyle', '--', 'LineWidth', 1, 'Color', [.5 .5 .5])
        hold on
        scatter(zhelp(1),zhelp(2),100,'Marker','+','LineWidth', 1,'MarkerEdgeColor', [.5 .5 .5])
        hold on
    end
end

% Special kite:
zhelp = zkite(:,7);
paramshelp = [1 .5 zhelp.' 135 3];

[x_kite,~,~,~] = kurve(100, 'kite', paramshelp);
x_kite = [x_kite;x_kite(1,:)];
plot(x_kite(:,1),x_kite(:,2),'LineWidth', 1.5, 'Color', 'blue')
hold on
plot(5*cos(d)+zhelp(1) ,5*sin(d)+zhelp(2),'LineStyle', '--', 'LineWidth', 1.2, 'Color', 'black')
hold on
scatter(zhelp(1),zhelp(2),100,'Marker','+','LineWidth', 1.2,'MarkerEdgeColor', 'black')
hold on


title('Geometry and a priori information for varying distance', 'Interpreter', 'Latex')
grid on
axis([-20 35.5 -30 10])

text(26.2,-1.8,'$D_1$','Color','blue','FontSize',16, 'Interpreter','latex')
text(-11.7,-19.5,'$D_2$','Color','blue','FontSize',16, 'Interpreter','latex')

ax = gca;
ax.FontSize = 16;

print ../figures/geometry_vary_distance.eps -depsc

%% Example 5.1 (Splitting only) for varying size:

clear all

% Parameters:

k = .5;  % wave number

sampling.nxhat = 2^8;  % number of observation directions, i.e. number of rows 
sampling.nd = 2^8; % number of illumination directions, i.e. number of columns
sampling.xhat = (0:sampling.nxhat-1)'/sampling.nxhat*2*pi; % detector positions
sampling.d = (0:sampling.nd-1)/sampling.nd*2*pi; % illumination directions

nr_of_scatterers = 2;
q = [-.5, 1];

noiselevel = 0.05;

% Splitting only:
[P_Omega, P_OmegaC] = generateOmega({}, sampling.nxhat, sampling.nd, [0 0 0 0 0]);

% Vary Size of one scatterer:
R_kite = 5;
R_nut = [2 3.5 5 6.5 8 9.5];

scaling_nut = R_nut*3/3.4;

znut = [26;-3];
v = [-2;-1]/norm([-2;-1]);
zkite = round(znut+40*v);

nr_rep = length(R_nut);

% Simulate far field operators:

G = cell(1,nr_rep);
Fnut = cell(1,nr_rep);
Fkite = cell(1,nr_rep);
Fall = cell(1,nr_rep);

for iteri = 1:nr_rep
    params = [1 .5 znut.' 45 scaling_nut(iteri); 1 .5 [zkite-[.5;-.5]].' 135 3];

    [Fnut{iteri}, ~, ~] = evaluateFarfieldNystrom({'nut'}, params(1,:), q(1), k, sampling.nxhat, 0);
    [Fkite{iteri}, ~, ~] = evaluateFarfieldNystrom({'kite'}, params(2,:), q(2), k, sampling.nxhat, 0);
    [Fall{iteri}, ~, ~] = evaluateFarfieldNystrom({'nut', 'kite'}, params, q, k, sampling.nxhat, 0);
    
    clear params

    rng("default")
    G{iteri} = P_OmegaC .* addNoise(Fall{iteri}, noiselevel);
end

% Numerical reconstruction:

tol = 1e-8;
kmax = 300;

Anut = cell(1,nr_rep);
Akite = cell(1,nr_rep);
Aall = cell(1,nr_rep);
B = cell(1,nr_rep);

indOfInterest = sub2ind([nr_of_scatterers,nr_of_scatterers],1:nr_of_scatterers,1:nr_of_scatterers);

for iteri = 1:nr_rep
   z = [znut, zkite];
   R = [R_nut(iteri), R_kite];
   
   [A, ~, ~] = CG_secondOrder(G{iteri}, sampling, k, P_Omega, z, R, kmax, tol);

   Aall{iteri} = A{2} + A{3} + A{4} + A{5};
   A = {A{1},A{indOfInterest+1}};
   B{iteri} = A{1};
   Anut{iteri} = A{2};
   Akite{iteri} = A{3};

   clear A z

   relerrnut2_cg(iteri) = norm(Anut{iteri}-Fnut{iteri})/norm(Fnut{iteri});
   relerrkite2_cg(iteri) = norm(Akite{iteri}-Fkite{iteri})/norm(Fkite{iteri});
end

% Numerical reconstruction with l1xl1-minimization:

kmax = 300;

Anut = cell(1,nr_rep);
Akite = cell(1,nr_rep);
Aall = cell(1,nr_rep);
B = cell(1,nr_rep);

for iteri = 1:nr_rep
    z = [znut, zkite];

    [A, ~, ] = FISTA_secondOrder(G{iteri}, sampling, k, P_OmegaC, z, kmax);
    Aall{iteri} = A{1};
    Anut{iteri} = A{2};
    Akite{iteri} = A{3};

    clear A z

    relerrnut2_fista(iteri) = norm(Anut{iteri}-Fnut{iteri})/norm(Fnut{iteri});
    relerrkite2_fista(iteri) = norm(Akite{iteri}-Fkite{iteri})/norm(Fkite{iteri});
end

% Plot relative errors:

figure()

semilogy(R_nut, relerrnut2_cg, 'r--o', R_nut, relerrkite2_cg, 'b--*', R_nut, relerrnut2_fista, 'r-o', R_nut, relerrkite2_fista, 'b-*', 'LineWidth', 1.2)

title('Relative errors for varying size', 'Interpreter', 'latex')
xlabel('$R_1$', 'Interpreter', 'latex')
ylabel('$\epsilon_{\mathrm{rel}}$', 'Interpreter', 'latex')

legend({'$\epsilon_{\mathrm{rel}}^1$ using cg','$\epsilon_{\mathrm{rel}}^2$ using cg', '$\epsilon_{\mathrm{rel}}^1$ using FISTA', '$\epsilon_{\mathrm{rel}}^2$ using FISTA'}, 'Interpreter','latex', 'Location','southeast')

xlim([R_nut(1) R_nut(end)])

grid on

ax = gca;
ax.FontSize = 16;

print ../figures/errors_vary_radius.eps -depsc

%% Plot of geometry:

figure()

d = (0:100)/100*2*pi;

% Kite:
[x_kite,~,~,~] = kurve(100, 'kite', [1 .5 [zkite-[.5;-.5]].' 135 3]);
x_kite = [x_kite;x_kite(1,:)];
plot(x_kite(:,1),x_kite(:,2),'LineWidth', 1.5, 'Color', 'blue')
hold on
plot(5*cos(d)+zkite(1) ,5*sin(d)+zkite(2),'LineStyle', '--', 'LineWidth', 1.2, 'Color', 'black')
hold on
scatter(zkite(1), zkite(2),100,'Marker','+','LineWidth', 1.2,'MarkerEdgeColor', 'black')
hold on

% Other nuts:
for iteri = 1:nr_rep
    paramshelp = [1 .5 znut.' 45 scaling_nut(iteri)];

    [x_nut,~,~,~] = kurve(100,'nut', paramshelp);
    x_nut = [x_nut;x_nut(1,:)];
    plot(x_nut(:,1),x_nut(:,2),'LineWidth', 1.5, 'Color', [0.678 0.847 0.902])
    hold on
    plot(R_nut(iteri)*cos(d)+znut(1),R_nut(iteri)*sin(d)+znut(2),'LineStyle', '--', 'LineWidth', 1, 'Color', [.5 .5 .5])
end

% Special nut:
% paramshelp = [(R_nut(3)^2*1) (R_nut(3)^2*.5) znut.' 45 3];
paramshelp = [1 .5 znut.' 45 scaling_nut(3)];

[x_nut,~,~,~] = kurve(100,'nut', paramshelp);
x_nut = [x_nut;x_nut(1,:)];
plot(x_nut(:,1),x_nut(:,2),'LineWidth', 1.5, 'Color', 'blue')
hold on
% plot(3.2*R_nut(3)*cos(d)+znut(1),3.2*R_nut(3)*sin(d)+znut(2),'LineStyle', '--', 'LineWidth', 1.2, 'Color', 'black')
plot(R_nut(3)*cos(d)+znut(1),R_nut(3)*sin(d)+znut(2),'LineStyle', '--', 'LineWidth', 1.2, 'Color', 'black')
hold on
scatter(znut(1), znut(2),100,'Marker','+','LineWidth', 1.2,'MarkerEdgeColor', 'black')

title('Geometry and a priori information for varying size', 'Interpreter', 'Latex')
grid on
axis([-20 35.5 -30 10])

text(26.2,-1.8,'$D_1$','Color','blue','FontSize',16, 'Interpreter','latex')
text(-12.5,-19.5,'$D_2$','Color','blue','FontSize',16, 'Interpreter','latex')

ax = gca;
ax.FontSize = 16;

print ../figures/geometry_vary_radius.eps -depsc