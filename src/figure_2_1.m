clear all
close all
clc

k = 1;
R1 = 10;
R2 = 100;

M = 1000;

N1end = 25;
N1_values = 0:N1end;
N2end = 150;
N2_values = 0:N2end;

sn1 = evaluate_sn(R1,k,0:M);
sn2 = evaluate_sn(R2,k,0:M);

sum1 = zeros(1,N1end+1);
sum2 = zeros(1,N2end+1);

for iterN = N1_values
    sum1(iterN+1) = 2*sum(sn1(iterN+1:end));
end

for iterN = N2_values
    sum2(iterN+1) = 2*sum(sn2(iterN+1:end));
end

relsum1 = sum1 ;% / (sn1(1)+2*sum(sn1(2:end)));
relsum2 = sum2 ;% / (sn2(1)+2*sum(sn2(2:end)));

%%
figure()
semilogy(N1_values,relsum1,'b-','linewidth',2)
hold on
title('$kR=10$', 'Interpreter', 'LaTex')
xlabel('$N$', 'Interpreter', 'LaTex')
% ylabel('$\sum_{|n|>N}\|J_n(kR)\|^2$', 'Interpreter', 'LaTex')
set(gca,'Fontsize',20)
set(gca, 'Linewidth', 1.5);
lims = axis;
fill([lims(1) lims(1) k*R1 k*R1], [lims(3) lims(4) lims(4) lims(3)], [0.7, 0.7, 0.7], 'FaceAlpha', 0.4, 'EdgeColor', 'none');
hold off
grid on

print decay_reminder_sum1.eps -depsc

figure()
semilogy(N2_values,relsum2,'b-','linewidth',2)
hold on
title('$kR=100$', 'Interpreter', 'LaTex')
xlabel('$N$', 'Interpreter', 'LaTex')
% ylabel('$\sum_{|n|>N}\|J_n(kR)\|^2$', 'Interpreter', 'LaTex')
set(gca,'Fontsize',20)
set(gca, 'Linewidth', 1.5);
lims = axis;
fill([lims(1) lims(1) k*R2 k*R2], [lims(3) lims(4) lims(4) lims(3)], [0.7, 0.7, 0.7], 'FaceAlpha', 0.4, 'EdgeColor', 'none');
hold off
grid on

print decay_reminder_sum2.eps -depsc

%%

function y = evaluate_sn(R,k,n)
    y = pi*(k*R)^2*((besselj(n,k*R)).^2 - besselj(n-1,k*R).*besselj(n+1,k*R));
end