clear all
close all 
clc

k = 5;  % wave number

M = 2^8;
sampling.nxhat = M;  % number of detector positions, i.e. number of rows 
sampling.nd = M; % number of illumination directions, i.e. number of columns
sampling.xhat = (0:sampling.nxhat-1)'/sampling.nxhat*2*pi; % detector positions
sampling.d = (0:sampling.nd-1)/sampling.nd*2*pi; % illumination directions

q = 2;

parkite = [1.5 .65 4 8 45 1];
[Fkite, ~, ~] = evaluateFarfieldNystrom({'kite'}, parkite, q, k, sampling.nxhat, 0);
zkite = [4;8];
Rkite = [2.2];


%%

s = [50 50 550 550];
figure('Renderer', 'painters', 'Position', s)

N = 13;

FFkite = 1/M^2*fftshift(fft2(translOp(Fkite, sampling, zkite, zkite, k)));
FFkite = [FFkite, FFkite(:,1)];
FFkite = [FFkite; FFkite(1,:)];
FFkite = flipud(FFkite);

imagesc((-32:32),(-32:32),abs(FFkite(M/2+1-32:M/2+1+32,M/2+1-32:M/2+1+32)))
set(gca,'ColorScale','log')
cx = caxis;
caxis([10^(-3) cx(2)])

hold on 
x1 = [-N:N,N*ones(1,2*N-1),N:-1:-N,-N*ones(1,2*N)];
x2 = [-N*ones(1,2*N+1),-N+1:N-1,N*ones(1,2*N+1),N-1:-1:-N];

hold on
plot(x1,x2,'--k', 'LineWidth', 1.5)

load('MyCMap.mat')
cmp = CMap;

colormap(cmp);

set(gca,'XTick',[-30 -20 -10 0 10 20 30]);
set(gca,'YTick',[-30 -20 -10 0 10 20 30]);

grid on

xlabel('$m$', 'Interpreter', 'LaTex', 'Fontsize', 21)
ylabel('$n$', 'Interpreter', 'LaTex', 'Fontsize', 21)

axis square


colorbar

title('Expansion coefficients $(|a_{m,n}|)_{m,n}$', 'Interpreter', 'Latex')
set(gca,'Fontsize',15)
set(gca,'YDir','normal')


print ../figures/figure_exp_coeff.eps -depsc

%%

figure('Renderer', 'painters', 'Position', s)

d = (0:100)/100*2*pi;

[x_kite,~,~,~] = kurve(100, 'kite', parkite);
x_kite = [x_kite;x_kite(1,:)];
plot(x_kite(:,1),x_kite(:,2),'LineWidth', 1.5, 'Color', 'blue')
hold on
plot(Rkite*cos(d)+zkite(1) ,Rkite*sin(d)+zkite(2),'LineStyle', '--', 'LineWidth', 1, 'Color', 'black')
hold on
scatter(zkite(1),zkite(2),100,'Marker','+','LineWidth', 1,'MarkerEdgeColor', 'black')

title('Scatterer $D$ and ball $B_R(c)$', 'Interpreter', 'Latex')

axis([0 11 0 11])
grid on
ax = gca;
ax.FontSize = 22;

xlabel('$x_1$', 'Interpreter', 'LaTex', 'Fontsize', 22)
ylabel('$x_2$', 'Interpreter', 'LaTex', 'Fontsize', 22)

print ../figures/figure_geometry.eps -depsc