clear all
close all 
clc

k = 5;  % wave number

M = 2^8;
sampling.nxhat = M;  % number of detector positions, i.e. number of rows 
sampling.nd = M; % number of illumination directions, i.e. number of columns
sampling.xhat = (0:sampling.nxhat-1)'/sampling.nxhat*2*pi; % detector positions
sampling.d = (0:sampling.nd-1)/sampling.nd*2*pi; % illumination directions

q = [-.5,2];

parkite = [1.5 .65 4 8 45 1];
parnut = [1 .5 8 2 105 1];
zkite = [4;8];
znut = [8;2];
Rkite = [2.2];
Rnut = [1.1];

%%

[Fkitenut] = evaluateFarfieldSecondOrder(k, sampling, {'kite', 'nut'}, [parkite; parnut], [11], q);

%%

s = [50 50 550 550];
figure('Renderer', 'painters', 'Position', s)

N1 = 13;
N2 = 7;

FFkitenut = 1/M^2*fftshift(fft2(translOp(Fkitenut, sampling, znut, zkite, k)));         % VERTAUSCHT
FFkitenut = [FFkitenut, FFkitenut(:,1)];
FFkitenut = [FFkitenut; FFkitenut(1,:)];
FFkitenut = flipud(FFkitenut);

imagesc((-32:32),(-32:32),abs(FFkitenut(M/2+1-32:M/2+1+32,M/2+1-32:M/2+1+32)))
hold on 
x1 = [-N2:N2,N2*ones(1,2*N1-1),N2:-1:-N2,-N2*ones(1,2*N1)];
x2 = [-N1*ones(1,2*N2+1),-N1+1:N1-1,N1*ones(1,2*N2+1),N1-1:-1:-N1];

hold on
plot(x2,x1,'--k', 'LineWidth', 1.5)                                                         % VERTAUSCHT

load('MyCMap.mat')
cmp = CMap;

colormap(cmp);

set(gca,'ColorScale','log')
cx = caxis;
caxis([10^(-3) cx(2)])

set(gca,'XTick',[-30 -20 -10 0 10 20 30]);
set(gca,'YTick',[-30 -20 -10 0 10 20 30]);

grid on

xlabel('$m$', 'Interpreter', 'LaTex', 'Fontsize', 21)
ylabel('$n$', 'Interpreter', 'LaTex', 'Fontsize', 21)

axis square


colorbar

title('Expansion coefficients $(|a_{m,n}^{(2)}|)_{m,n}$', 'Interpreter', 'Latex')
set(gca,'Fontsize',21)
set(gca,'YDir','normal')


print ../figures/figure_exp_coeff_kiteandnut.eps -depsc

%%

s = [50 50 550 550];
figure('Renderer', 'painters', 'Position', s)

d = (0:100)/100*2*pi;

[x_kite,~,~,~] = kurve(100, 'kite', parkite);
x_kite = [x_kite;x_kite(1,:)];

[x_nut,~,~,~] = kurve(100, 'nut', parnut);
x_nut = [x_nut;x_nut(1,:)];

plot(x_kite(:,1),x_kite(:,2),'LineWidth', 1.5, 'Color', 'blue')
hold on
plot(x_nut(:,1),x_nut(:,2),'LineWidth', 1.5, 'Color', 'blue')
hold on

plot(Rkite*cos(d)+zkite(1) ,Rkite*sin(d)+zkite(2),'LineStyle', '--', 'LineWidth', 1, 'Color', 'black')
hold on
scatter(zkite(1),zkite(2),100,'Marker','+','LineWidth', 1,'MarkerEdgeColor', 'black')

plot(Rnut*cos(d)+znut(1) ,Rnut*sin(d)+znut(2),'LineStyle', '--', 'LineWidth', 1, 'Color', 'black')
hold on
scatter(znut(1),znut(2),100,'Marker','+','LineWidth', 1,'MarkerEdgeColor', 'black')

title('Scatterers $D_j$ and balls $B_{R_j}(c_j)$, $j=1,2$', 'Interpreter', 'Latex')

text(4.15,8.35,'$D_1$','Color','blue','FontSize',22, 'Interpreter','latex')
text(7.6,2.45,'$D_2$','Color','blue','FontSize',22, 'Interpreter','latex')

axis([0 11 0 11])
grid on
ax = gca;
ax.FontSize = 22;

xticks([0,2,4,6,8,10])
yticks([0,2,4,6,8,10])

xlabel('$x_1$', 'Interpreter', 'LaTex', 'Fontsize', 22)
ylabel('$x_2$', 'Interpreter', 'LaTex', 'Fontsize', 22)

print ../figures/figure_geometry_kiteandnut.eps -depsc