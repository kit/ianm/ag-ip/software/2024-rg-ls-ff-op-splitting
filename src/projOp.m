function [Ft] = projOp(F, sampling, kappa, c1, c2, N1, N2)

%% PROJOP: Simulates projection operator on generalized subspace of non-evanescent far field
% operators. Projects of F onto the subspace $T_{c_1,c_2}^\astV_{N_1,N_2}$.
%
% INPUT:    F           Far field matrix, nxhat*nd-array.
%           sampling    Structure containing information about the discretization.
%           kappa       Wave number, >0.
%           c1          Determines translation in row direction, vector of length 2.
%           c2          Determines translation in column direction, vector of length 2.
%           N1          Cutting index in row direction, natural number.
%           N2          Cutting index in column direction, natural number.
%
% OUTPUT:   Fp      Projected far field matrix, nxhat*nd-array.
% 
% SYNTAX: projOp(F, sampling, kappa, c1, c2, N1, N2)
%
% *********************************************************************************************

F = translOp(F, sampling, c1, c2, kappa); % shift the origin

Ffft = fft2(F);

Ffft(:,(N1+2) : (end-N1)) = 0;
Ffft((N2+2) : (end-N2),:) = 0;

F = ifft2(Ffft);

Fp = translOp(F, sampling, -c1, -c2, kappa); % shift back the origin

end
