%% Example 5.2 (Completion only) for Omega_1:

clear all
close all
clc

% Parameters:

k = .5;  % wave number

sampling.nxhat = 2^8;  % number of observation directions, i.e. number of rows 
sampling.nd = 2^8; % number of illumination directions, i.e. number of columns
sampling.xhat = (0:sampling.nxhat-1)'/sampling.nxhat*2*pi; % detector positions
sampling.d = (0:sampling.nd-1)/sampling.nd*2*pi; % illumination directions

nr_of_scatterers = 2;
q = [-.5, 1];

noiselevel = 0.05;

alpha_vec = pi/32*[1,2,3,4,5,6,7,8,9,10]; % controlls area of non-observable sets

nr_rep = length(alpha_vec);

P_Omega = cell(1,nr_rep);
P_OmegaC = cell(1,nr_rep);

for iteri=1:nr_rep
    
    alpha = alpha_vec(iteri);

    % cross-shaped Omega, 20% midding data:
    OmegaTypes = {'constant_height','constant_width'};
    par = [pi/3, 0, alpha, 0, 0; 0, 4*pi/3, 0, alpha, 0];

    [Help, ~] = generateOmega(OmegaTypes, sampling.nxhat, sampling.nd, par);

    % To enforce symmetry according to reciprocity principle:
    P_Omega{iteri} = applyRP(Help);
    P_OmegaC{iteri} = ones(sampling.nd,sampling.nxhat) - P_Omega{iteri};

    ratio(iteri) = sum(sum(P_Omega{iteri}))/(sampling.nxhat*sampling.nd);

    clear Help
end

% Simulate far field operators:

znut = [26;-3];
v = [-2;-1]/norm([-2;-1]);
dist = 20;
zkite = znut+dist*v;

z = round(znut+9.75*v); % centre of of ball containing all scatterer components
R = [13];  % radii of ball containing all scatterer components

G = cell(1,nr_rep);

params = [1 .5 znut.' 45 3; 1 .5 zkite.' 135 3];

[Fall, ~, ~] = evaluateFarfieldNystrom({'nut', 'kite'}, params, q, k, sampling.nxhat, 0);
    
for iteri = 1:nr_rep
    rng("default")
    G{iteri} = P_OmegaC{iteri} .* addNoise(Fall, noiselevel);
end

% Numerical reconstruction with least squares approach:

tol = [1e-5 1e-5 1e-5 1e-4 1e-4 5e-3 5e-3 5e-3 1e-3 1e-3];
kmax = 300;

Aall = cell(1,nr_rep);
B = cell(1,nr_rep);

for iteri = 1:nr_rep
   [A, ~, ~]= CG_secondOrder(G{iteri}, sampling, k, P_Omega{iteri}, z, R, kmax, tol(iteri));

   Aall{iteri} = A{2};
   B{iteri} = A{1};
   clear A

   relerrOmega1_cg(iteri) = norm(B{iteri}+P_Omega{iteri}.*Fall)/norm(P_Omega{iteri}.*Fall);
   relerr1_cg(iteri) = norm(Aall{iteri}-Fall)/norm(Fall);
end

% Numerical reconstruction with l1xl1-minimization:

kmax = 300;

Aall = cell(1,nr_rep);
B = cell(1,nr_rep);

for iteri = 1:nr_rep
    [A, ~, ~] = FISTA_secondOrder(G{iteri}, sampling, k, P_OmegaC{iteri}, z, kmax);
    Aall{iteri} = A{1};
    clear A

    relerrOmega1_fista(iteri) = norm(P_Omega{iteri}.*(Aall{iteri}-Fall))/norm(P_Omega{iteri}.*Fall);
    relerr1_fista(iteri) = norm(Aall{iteri}-Fall)/norm(Fall);
end

%% Plot of relative errors for Omega_1:

figure()

semilogy(ratio, relerrOmega1_cg, 'r--o', ratio, relerr1_cg, 'b--*', ratio, relerrOmega1_fista, 'r-o', ratio, relerr1_fista, 'b-*', 'LineWidth', 1.2)

title('Relative errors for varying area of non-observable set $\Omega_1$', 'Interpreter', 'latex')
xlabel('$\frac{|\Omega|}{4\pi^2}$', 'Interpreter', 'latex')
ylabel('$\epsilon_{\mathrm{rel}}$', 'Interpreter', 'latex')

legend({'$\epsilon_{\mathrm{rel}}^{\Omega}$ using cg','$\epsilon_{\mathrm{rel}}$ using cg', '$\epsilon_{\mathrm{rel}}^{\Omega}$ using FISTA', '$\epsilon_{\mathrm{rel}}$ using FISTA'}, 'Interpreter','latex', 'Location','southeast')

xlim([ratio(1) ratio(end)])
ylim([10^(-2) 1])

grid on

ax = gca;
ax.FontSize = 16;

print ../figures/errors_vary_Omega1_completion.eps -depsc

%% Plot of geometry:

figure()

d = (0:100)/100*2*pi;

[x_nut,~,~,~] = kurve(100,'nut', [1 .5 znut.' 45 3]);
x_nut = [x_nut;x_nut(1,:)];
plot(x_nut(:,1),x_nut(:,2),'LineWidth', 1.5, 'Color', 'blue')
hold on
scatter(z(1,1),z(2,1),100,'Marker','+','LineWidth', 1.2,'MarkerEdgeColor', 'black')
hold on
plot(13*cos(d)+z(1,1),13*sin(d)+z(2,1),'LineStyle', '--', 'LineWidth', 1.2, 'Color', 'black')
hold on
[x_kite,~,~,~] = kurve(100, 'kite', [1 .5 zkite.' 135 3]);
x_kite = [x_kite;x_kite(1,:)];
plot(x_kite(:,1),x_kite(:,2),'LineWidth', 1.5, 'Color', 'blue')
hold on

% small balls:
z = [znut, round(zkite)];
R = [3.5, 5];

plot(R(1)*cos(d)+z(1,1) ,R(1)*sin(d)+z(2,1),'LineStyle', ':', 'LineWidth', 1.2, 'Color', 'black')
hold on
scatter(z(1,1),z(2,1),100,'Marker','+','LineWidth', 1.2,'MarkerEdgeColor', 'black')
hold on
plot(R(2)*cos(d)+z(1,2) ,R(2)*sin(d)+z(2,2),'LineStyle', ':', 'LineWidth', 1.2, 'Color', 'black')
hold on
scatter(z(1,2),z(2,2),100,'Marker','+','LineWidth', 1.2,'MarkerEdgeColor', 'black')

title('Geometry and a priori information', 'Interpreter', 'Latex')
grid on
axis equal

text(26.2,-1.8,'$D_1$','Color','blue','FontSize',18, 'Interpreter','latex')
text(6.25,-10.8,'$D_2$','Color','blue','FontSize',18, 'Interpreter','latex')

ax = gca;
ax.FontSize = 18;

print ../figures/geometry_vary_Omega.eps -depsc

%% Plot of Omega_1:

figure()

imagesc(sampling.d,(0:sampling.nxhat)/sampling.nxhat*2*pi,P_Omega{2})
hold on

axis([0 2*pi 0 2*pi])
set(gca,'XTick',[0,pi/2,pi,3*pi/2,2*pi])
set(gca,'XTickLabel',{'0','\pi/2','\pi','3\pi/2','2\pi'})
set(gca,'YTick',[0,pi/2,pi,pi*3/2,2*pi]);
set(gca,'YTickLabel',{'0','\pi/2','\pi','3\pi/2','2\pi'})

axis square

map = [1 1 1;0 0 0];
colormap(map)

phib = pi/3;
phie = pi/3 + alpha_vec(2);
thetab = 4*pi/3;
thetae = 4*pi/3 + alpha_vec(2);

plot([0,thetab],[phib,phib],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
hold on
plot([thetab,thetab],[pi/3 + alpha_vec(1),2*pi],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
hold on
plot([thetab,thetab],[0,phib],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
hold on
plot([4*pi/3 + alpha_vec(1),2*pi],[phib,phib],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
hold on
for iteri = 1:nr_rep
    thetae = thetab + alpha_vec(iteri);
    phie = phib + alpha_vec(iteri);

    plot([thetae,thetae],[0,phib],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
    hold on
    plot([thetae,thetae],[phie,2*pi],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
    hold on
    plot([0,thetab],[phie,phie],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
    hold on
    plot([thetae,2*pi],[phie,phie],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
end

title('Non-observable set $\Omega_1$', 'Interpreter', 'Latex')

set(gca,'Fontsize',18)
set(gca,'YDir','normal')

print ../figures/support_Omega1.eps -depsc

%% Example 5.2 (Completion only) for Omega_2:

clear all

% Parameters:

k = .5;  % wave number

sampling.nxhat = 2^8;  % number of observation directions, i.e. number of rows 
sampling.nd = 2^8; % number of illumination directions, i.e. number of columns
sampling.xhat = (0:sampling.nxhat-1)'/sampling.nxhat*2*pi; % detector positions
sampling.d = (0:sampling.nd-1)/sampling.nd*2*pi; % illumination directions

nr_of_scatterers = 2;
q = [-.5, 1];

noiselevel = 0.05;

alpha_vec = pi/32*[1,2,3,4,5,6,7,8,9,10]; % controlls area of non-observable sets

nr_rep = length(alpha_vec);

P_Omega = cell(1,nr_rep);
P_OmegaC = cell(1,nr_rep);

for iteri=1:nr_rep

    alpha = alpha_vec(iteri);

    % Omega consisting of three components, 20% missing data:
    OmegaTypes = {'rectangle','rectangle','rectangle'};
    par = [pi/3, 0, alpha, pi, 0; pi, 4*pi/3, pi, alpha, 0; (3*pi-sqrt(alpha*(2*pi-alpha)))/2, (pi-sqrt(alpha*(2*pi-alpha)))/2, sqrt(alpha*(2*pi-alpha)), sqrt(alpha*(2*pi-alpha)), 0];

    [Help, ~] = generateOmega(OmegaTypes, sampling.nxhat, sampling.nd, par);

    % To enforce symmetry according to reciprocity principle:
    P_Omega{iteri} = applyRP(Help);
    P_OmegaC{iteri} = ones(sampling.nd,sampling.nxhat) - P_Omega{iteri};

    ratio(iteri) = sum(sum(P_Omega{iteri}))/(sampling.nxhat*sampling.nd);

    clear Help
end

% Simulate far field operators:

znut = [26;-3];
v = [-2;-1]/norm([-2;-1]);
dist = 20;
zkite = znut+dist*v;

z = round(znut+9.75*v); % centre of of ball containing all scatterer components
R = [13];  % radii of ball containing all scatterer components

G = cell(1,nr_rep);

params = [1 .5 znut.' 45 3; 1 .5 zkite.' 135 3];

[Fall, ~, ~] = evaluateFarfieldNystrom({'nut', 'kite'}, params, q, k, sampling.nxhat, 0);
    
for iteri = 1:nr_rep
    rng("default")
    G{iteri} = P_OmegaC{iteri} .* addNoise(Fall, noiselevel);
end

% Numerical reconstruction with least squares approach:

tol = [1e-5 1e-5 1e-5 1e-4 5e-3 5e-3 5e-3 5e-3 1e-3 1e-3];
kmax = 300;

Aall = cell(1,nr_rep);
B = cell(1,nr_rep);

for iteri = 1:nr_rep
   [A, ~, ~] = CG_secondOrder(G{iteri}, sampling, k, P_Omega{iteri}, z, R, kmax, tol(iteri));

   Aall{iteri} = A{2};
   B{iteri} = A{1};
   clear A

   relerrOmega2_cg(iteri) = norm(B{iteri}+P_Omega{iteri}.*Fall)/norm(P_Omega{iteri}.*Fall);
   relerr2_cg(iteri) = norm(Aall{iteri}-Fall)/norm(Fall);
end

% Numerical reconstruction with l1xl1-minimization:

kmax = 300;

Aall = cell(1,nr_rep);
B = cell(1,nr_rep);

for iteri = 1:nr_rep
    [A, ~, ~] = FISTA_secondOrder(G{iteri}, sampling, k, P_OmegaC{iteri}, z, kmax);
    Aall{iteri} = A{1};
    clear A

    relerrOmega2_fista(iteri) = norm(P_Omega{iteri}.*(Aall{iteri}-Fall))/norm(P_Omega{iteri}.*Fall);
    relerr2_fista(iteri) = norm(Aall{iteri}-Fall)/norm(Fall);
end

%% Plot of relative errors for Omega_2:

figure()

semilogy(ratio, relerrOmega2_cg, 'r--o', ratio, relerr2_cg, 'b--*', ratio, relerrOmega2_fista, 'r-o', ratio, relerr2_fista, 'b-*', 'LineWidth', 1.2)

title('Relative errors for varying area of non-observable set $\Omega_2$', 'Interpreter', 'latex')
xlabel('$\frac{|\Omega|}{4\pi^2}$', 'Interpreter', 'latex')
ylabel('$\epsilon_{\mathrm{rel}}$', 'Interpreter', 'latex')

legend({'$\epsilon_{\mathrm{rel}}^{\Omega}$ using cg','$\epsilon_{\mathrm{rel}}$ using cg', '$\epsilon_{\mathrm{rel}}^{\Omega}$ using FISTA', '$\epsilon_{\mathrm{rel}}$ using FISTA'}, 'Interpreter','latex', 'Location','southeast')

xlim([ratio(1) ratio(end)])
ylim([10^(-2) 1])

grid on

ax = gca;
ax.FontSize = 16;

print ../figures/errors_vary_Omega2_completion.eps -depsc

%% Plot of Omega_2:

figure()

imagesc(sampling.d,(0:sampling.nxhat)/sampling.nxhat*2*pi,P_Omega{2})
hold on

axis([0 2*pi 0 2*pi])
set(gca,'XTick',[0,pi/2,pi,3*pi/2,2*pi])
set(gca,'XTickLabel',{'0','\pi/2','\pi','3\pi/2','2\pi'})
set(gca,'YTick',[0,pi/2,pi,pi*3/2,2*pi]);
set(gca,'YTickLabel',{'0','\pi/2','\pi','3\pi/2','2\pi'})

axis square

map = [1 1 1;0 0 0];
colormap(map)

% Two Rectangles:

phib = pi/3;
phie = pi/3 + alpha_vec(6);
thetab = 4*pi/3;
thetae = 4*pi/3 + alpha_vec(6);

plot([0,pi],[phib,phib],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
hold on
plot([thetab,thetab],[pi,2*pi],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
hold on
plot([0,0],[phib,phie],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
hold on
plot([pi,pi],[phib,phie],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
hold on
plot([thetab,thetae],[pi,pi],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
hold on
plot([thetab,thetae],[2*pi,2*pi],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
hold on

for iteri = 1:nr_rep

    phie = phib + alpha_vec(iteri);
    thetae = thetab + alpha_vec(iteri);

    plot([0,pi],[phie,phie],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
    hold on
    plot([thetae,thetae],[pi,2*pi],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
    hold on
end

% Square:

for iteri = 1:nr_rep
    beta = sqrt(alpha_vec(iteri)*(2*pi-alpha_vec(iteri)));
    phib = (3*pi-beta)/2;
    phie = (3*pi+beta)/2;
    thetab = (pi-beta)/2;
    thetae = (pi+beta)/2;

    plot([thetab,thetae],[phib,phib],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
    hold on
    plot([thetab,thetae],[phie,phie],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
    hold on
    plot([thetab,thetab],[phib,phie],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
    hold on
    plot([thetae,thetae],[phib,phie],'LineWidth', 2, 'Color', [0.5 0.5 0.5],'LineStyle', '--')
end

title('Non-observable set $\Omega_2$', 'Interpreter', 'Latex')

set(gca,'Fontsize',18)
set(gca,'YDir','normal')

print ../figures/support_Omega2.eps -depsc