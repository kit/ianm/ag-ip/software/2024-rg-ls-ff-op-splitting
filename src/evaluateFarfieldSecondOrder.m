function [F] = evaluateFarfieldSecondOrder(k, sampling, objects, par, R, q)

%% EVALUATEFARFIELDSECONDORDER: Calculates a discretized version of the Born far field
% operator of second order by using the composite trapezoidal rule for the outer
% integral and calculations as suggested by Vainikko in order to approximate the
% individual Born farfields.
%
% INPUT: kappa      Wave number, >0.
%        sampling   Structure containing information about the discretization.
%        objects    Cell-array including two strings that set the shape-types of
%                   the two scatterers.
%        par        Vector containing all further information about the scatterers
%                   shape, 2*6-array.
%        R          Sizes of the two scatterers, i.e. radii of balls containing them,
%                   vector of 2.
%        q          Values of the contrast function within the individual scatterers,
%                   >-1.
%   
% OUTPUT: F     Born farfield operator of second order, nxhat*nd-array.
%
% SYNTAX: evaluateFarfieldSecondOrder(k, sampling, objects, par, R, q)
%
% ************************************************************************************

kR = k * R;

nd = sampling.nd; % number of illumination directions
nxhat = sampling.nxhat; % number of observation directions
xhat = sampling.xhat;
d = sampling.d;

doub = 8; % number of doublings for discretization
N = 2^doub; % number of discretizations in one dimension
h = 2*kR/N; % discretization step width

[X,Y] = meshgrid(-(N/2)*h:h:(N/2-1)*h,-(N/2)*h:h:(N/2-1)*h); % Discretization of [-R,R]^2
x = reshape(X,[N^2,1]);
y = reshape(Y,[N^2,1]);

q1 = qBorn(objects{1}, par(1,:), x/k, y/k);
Q1 = q(1) * reshape(q1,[N,N]);

q2 = qBorn(objects{2}, par(2,:), x/k, y/k);
Q2 = q(2) * reshape(q2,[N,N]);

% Kernel Khat:
Khat = zeros(N,N);
J0R1 = besselj(0,kR);
J1R1 = besselj(1,kR);
H10R1 = J0R1+1i*bessely(0,kR);
H11R1 = J1R1+1i*bessely(1,kR);
for j1=1:N
    for j2=1:N
        absj = sqrt((j1-N/2-1)^2+(j2-N/2-1)^2);
        
        if (j1==N/2+1) && (j2==N/2+1)
            Khat(j1,j2) = -1/(2*kR) + (1i*pi)/4*H11R1;
        elseif abs(pi*absj-kR)<10*eps
            Khat(j1,j2) = (1i*kR*pi)/8 * (J0R1*H10R1+J1R1*H11R1);
        else
            J0 = besselj(0,pi*absj);
            J1 = besselj(1,pi*absj);
            Khat(j1,j2) = kR^2/(pi^2*absj^2-kR^2) * 1/(2*kR) ...
                * (1 + (1i*pi)/2*(pi*absj*J1*H10R1-kR*J0*H11R1));
        end
        
    end
end
Khat = 2*kR * ifftshift(Khat);

% Calculate part of far field operator corresponding to second order scattering first on q1 then on q2:
F = zeros(nxhat,nd);

for iteri = 1:nd
    ui = uinc(x,y,d);
    Ui = reshape(ui(:,iteri),[N,N]);

    Vs1 = ifft2(ifftshift(fftshift(Khat).*fftshift(fft2(Q1.*Ui))));% fftshift(ifft2(Khat.*fft2(ifftshift(Q1.*Ui))));
    
    c = getc(N,h);
    Us1 = h^2 * ToepPhi(c,Q1.*Ui);
    % F as first order Born approximation of far field of Us1:
    Vcol = reshape(Q2.*Us1,(N)^2,1);
    Vhelp = repmat(Vcol,1,nxhat);
    E = exp(-1i*[x,y]*[cos(xhat), sin(xhat)].');
    Vhelp = h^2 * E .* Vhelp;
    farfield = sum(Vhelp,1).';
    F(:,iteri) = farfield;

end

F = 2*pi/nd * F; % adding weights of trapecoidal rule
    
end

function value = uinc(x,y,d)
    value = exp(1i*(x*cos(d) + y*sin(d)));
end

function [q_value] = qBorn(object, par, y1, y2)

% Idea: q is 1 if y=(y1,y2) lies inside the by object, par prescribed figure
% and zero otherwise.
% Shift (y1,y2) by (-par(3),-par(4)) and rotate it with angle
% -par(5)*pi/180. 

lambda = 0.5;
[n,~] = size(y1);
rot = par(5)*pi/180;
R = [ cos(-rot) -sin(-rot) ; sin(-rot) cos(-rot) ];
shift = repmat([par(3); par(4)],1,n);

[x,~,~,~] = kurve(sqrt(n), object, par);
[x_phi,x_rad] = cart2pol(x(:,1)-par(3)*ones(sqrt(n),1),x(:,2)-par(4)*ones(sqrt(n),1));

[y_phi, y_rad] = cart2pol(y1-par(3)*ones(n,1),y2-par(4)*ones(n,1));

help = repmat(x_phi,1,n) - repmat(y_phi.',sqrt(n),1);
[~,index] = min(abs(help),[],1);
clear help
q_value = (y_rad <= x_rad(index));
clear index
end

function [c,A] = getc(N,h,getA)

for jj = 1:N
    for kk = 1:N
        absj = sqrt( (jj-1)^2 + (kk-1)^2 );
        if jj == 1 && kk==1
            VecColumn(1) = 0;
        else
            VecColumn(N*(jj-1) + kk,1) = 1i/4*besselh(0,absj*h);
        end
    end
end
for ii = 1:N
    for jj = 1 : N
        absj = sqrt( (ii-1)^2 + (jj-1)^2 );
        if ii == 1 && jj==1
            ColumnForT(ii,jj) = 0;
        else
            ColumnForT(ii,jj) = 1i/4*besselh(0,absj*h);
        end
    end
end
for jj = 1:N
    for kk = 1:N
        absj = sqrt( (jj-1)^2 + (kk-1)^2 );
        if jj == 1 && kk==1
            VecRow(1) = 0;
        else
            VecRow(1,N*(jj-1) + kk) = 1i/4*besselh(0,absj*h);
        end
    end
end

if nargin == 3 && getA == 1
    % create first row blocks
    RowMats = {};
    for jj = 1:N
        RowMats{end+1} = toeplitz(ColumnForT(jj,:),[ColumnForT(jj,1), VecRow(N*(jj-1)+2:N*jj)]);
    end
    n=length(RowMats);
    A = cell2mat(RowMats(toeplitz(1:n)));
else
    A = [];
end
c = zeros(2*N,2*N);
c(1:N,1:N) = ColumnForT;
for jj = 1:N
    c(N+2:end,jj) = fliplr(VecRow(N*(jj-1)+2:N*jj));
end
c(:,N+2:end) = fliplr(c(:,2:N));
end

function Y = ToepPhi(c,X)
N = size(X,2);
xhat = fft2(c) .* fft2(X,2*N,2*N);
xmat = ifft2(xhat);
Y = xmat(1:N,1:N);
end
